from mutagen.easyid3 import EasyID3
import os

class Track:
    def __init__(self):
        pass



    def convertToMP3(self, album):
        try:
            os.system("avconv -i " + self.encodedName + ".flv" + " -b:a 320k " + self.encodedName + ".mp3")
            os.remove(self.encodedName + ".flv")
            self.tagTrack(album)
            os.system("mv " + self.encodedName +".mp3 " + album.primaryArtist + "/" + album.albumShortcut)
        except:
            print "[-]: Could not convert or remove FLV-file!"


    def tagTrack(self, album):
        try:
            audio = EasyID3(self.encodedName + ".mp3")
            audio['title'] = self.displayName
            audio["artist"] = self.artistName
            audio["album"] = album.displayName
            audio.save()
        except:
            print "[-]: Could not Tag File"

# Requirements
- sudo apt-get install python python-pip rtmpdump libav-tools
- sudo pip install mutagen
(not working with python3)

# USAGE
You can build a textfile of Napster Album urls.
The should look like:

  https://app.napster.com/artist/a-skylit-drive/album/adelphia-explicit


This file needs to have one link each line.
If you want to wait with a link to be downloaded, then you can put a "#"
at the beginning of the line, then this one gets ignored.

This application now catch the album tracks and their stream Location.

With this location the rtmpdump tool can download a .flv file, which get
converted into .mp3.

After all this the file should be tagged using mutagen.

Usage Example:
	python main.py example@mail.com mypassword albums.txt


# Help
Donate to:
BTC: 13soRCUWcwYBcQBEp96eiF8zHCqSdUHsKi
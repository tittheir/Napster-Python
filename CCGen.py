import requests
from random import Random
import copy

BINS = [
    ["5","1","9","6","0","3"],

]

COOKIES = {}


rand = Random()
rand.seed()


def generateCCNumber(binNumber, length):
    ccnumber = binNumber
    while len(ccnumber) < (length - 1):
        digit = str(rand.choice(range(0, 10)))
        ccnumber.append(digit)
    # Calculate sum
    sum = 0
    pos = 0
    reversedCCnumber = []
    reversedCCnumber.extend(ccnumber)
    reversedCCnumber.reverse()
    while pos < length - 1:
        odd = int(reversedCCnumber[pos]) * 2
        if odd > 9:
            odd -= 9
        sum += odd
        if pos != (length - 2):
            sum += int(reversedCCnumber[pos + 1])
        pos += 2
    # Calculate check digit
    checkdigit = ((sum / 10 + 1) * 10 - sum) % 10
    ccnumber.append(str(checkdigit))
    return ''.join(ccnumber)


def credit_card_number(rnd, length):
    binNumber = copy.copy(rnd.choice(BINS))
    ccnumber = generateCCNumber(binNumber, length)
    return ccnumber


print(credit_card_number(rand,16))

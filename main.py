import json
import requests
import sys
import os
import re
import time
from User import User
from Album import Album
import argparse
import subprocess

ALBUMSFILE = "albums.txt"
URL = "https://direct.rhapsody.com/"
RHAPSODY_HEADER_1 = {
    'Accept': 'application/json',
    'User-Agent': 'Mozilla/5.0 (X11; Windows NT5) Gecko/20100101 Firefox/45.0',
    'x-rds-devkey': '4B8C5B7B5B7B5I4H',
    'x-rds-cobrand': '40134',
    'Authorization': 'Basic cmhhcGNvbToyWTZIWTJxYlRURnltb1llcmZ1Wg==',
    'origin': 'https://app.napster.com'
}

RHAPSODY_HEADER = {
    'Accept': 'application/json',
    'User-Agent': 'Mozilla/5.0 (X11; Windows NT5) Gecko/20100101 Firefox/45.0',
    'origin': 'https://app.napster.com'
}
DEV_KEY = "5C8F8G9G8B4D0E5J"


def sendNotification():
    subprocess.Popen(['notify-send', "Finished Downloading!"])
    return




def login(usermail, password):
    url = URL + "/authserver/v3/useraccounts?userName=" + usermail
    headers = RHAPSODY_HEADER_1
    headers["x-rds-authentication"] = password
    while(1):
        try:
            resp = requests.get(url, headers=headers)
            break
        except requests.exceptions.ConnectionError:
            print "Connection refused"

    if resp.status_code == 200:
        json_resp = json.loads(resp.text)
        myUser = User()
        myUser.userName = json_resp["userName"]
        myUser.userId = json_resp["userId"]
        myUser.rhapsodyAccessToken = json_resp["rhapsodyAccessToken"]
        myUser.cocat = json_resp["cocat"]
        return myUser
    else:
        print resp.text
        sys.exit(2)




def getLogonToken(myUser):
    url = URL + "account/data/methods/getLogonToken.js"
    payload = {
        'logon': myUser.userName,
        'password': myUser.rhapsodyAccessToken,
        'cobrandId':40143,
        'developerKey':DEV_KEY
    }
    while(1):
        try:
            resp = requests.get(url, headers = RHAPSODY_HEADER, params=payload)
            break
        except requests.exceptions.ConnectionError:
            print "Connection Refused"

    if resp.status_code == 200:
        myUser.logonToken = json.loads(resp.text)["value"]
    else:
        print resp.text
        sys.exit(2)




def getAlbumID(myAlbum):
    url = URL + "/metadata/data/methods/getIdByShortcut.js"
    payload = {
        'albumShortcut': myAlbum.albumShortcut,
        'artistShortcut': myAlbum.artistShortcut,
        'developerKey' : DEV_KEY
    }
    while(1):
        try:
            resp = requests.get(url, headers = RHAPSODY_HEADER, params=payload)
            break
        except requests.exceptions.ConnectionError:
            print "Connection refused"

    if resp.status_code == 200:
        myAlbum.albumId = json.loads(resp.text)["id"]
    else:
        print resp.text
        sys.exit(2)




def getAlbumDetails(myAlbum, myUser):
    url = URL + "/metadata/data/methods/getAlbum.js"
    payload = {
        'albumId': myAlbum.albumId,
        'filterRightsKey': 2,
        'developerKey': DEV_KEY,
        'cobrandId': myUser.cocat
    }
    while(1):
        try:
            resp = requests.get(url, headers = RHAPSODY_HEADER, params=payload)
            break
        except requests.exceptions.ConnectionError:
            print "ConnectionError"

    if resp.status_code == 200:
        json_album_info = json.loads(resp.text)
        myAlbum.primaryArtist = re.sub(r'\W+', "_", json_album_info["primaryArtistDisplayName"])
        myAlbum.encodedName = re.sub(r'\W+', '_', json_album_info["displayName"])
        myAlbum.displayName = json_album_info["displayName"]
        myAlbum.addTracks(json_album_info["trackMetadatas"])
    else:
        print resp.text
        sys.exit(2)




def startPlaybackSession(myUser):
    url = URL + "/playbackserver/v1/users/"+ myUser.userId + "/sessions"
    headers = RHAPSODY_HEADER_1
    while(1):
        try:
            resp = requests.post(url, json={'clientType':'rhap-web'}, headers=headers)
            break
        except requests.exceptions.ConnectionError:
            print "Connection refused"

    if resp.status_code == 201:
        myUser.playbackSession = json.loads(resp.text)["id"]
    else:
        print resp.text
        sys.exit(2)




def getTrackLocation(myUser, track):
    url = URL + "/playbackserver/v1/users/" + myUser.userId +\
        "/sessions/" + myUser.playbackSession + \
        "/track/" + track.trackId + "?context=ON_DEMAND"
    headers = RHAPSODY_HEADER_1
    headers["x-rhapsody-access-token-v2"] = myUser.rhapsodyAccessToken
    while(1):
        try:
            resp = requests.get(url, headers = headers)
            break
        except requests.exceptions.ConnectionError:
            print "Connection Refused"

    if resp.status_code == 200:
        json_location = json.loads(resp.text)
        error = True
        for media in json_location["stationTrack"]["medias"]:
            if "m4a" in media["location"] and media["bitrate"]=="320":
                track.location = media["location"]
                error = False
                break
            elif "m4a" in media["location"] and media["bitrate"]=="192":
                track.location = media["location"]
                error = False
                break
            elif "m4a" in media["location"] and media["bitrate"]=="64":
                track.location = media["location"]
                error = False
                break
        if error:
            print json_location
            print "[-]: Error getting right Format! Maybe no MP4 available!"
            sys.exit(2)
    else:
        print resp.text
        print "[-]: Error getting Track Location"
        sys.exit()




def download(track, album):
    splitted_location = track.location.split("/")
    url = "/".join(splitted_location[:3])
    tcUrl = "/".join(splitted_location[:5])
    app = "/".join(splitted_location[3:5])
    playpath = "mp4:" + "/".join(splitted_location[5:])
    cmd = 'rtmpdump -r "' +\
            url +'"' + \
            ' --playpath "' + playpath +\
            '" -o ' + track.encodedName + ".flv" +\
            ' --app "' + app +\
            '" --tcUrl "' + tcUrl +\
            '" '
    os.system(cmd)
    time.sleep(3)


################################################################################
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("username", help="Username of your Napster Account")
    parser.add_argument("password", help="Your password to your napster account")
    parser.add_argument("albumfile", help="Path to album file")
    args = parser.parse_args()

    myUser = login(args.username, args.password)
    getLogonToken(myUser)


    if os.path.isfile(args.albumfile):
        f = open(args.albumfile, "r")
        for line in f.readlines():
            if not line.startswith("#") and line != "\n":
                # Make a new Album with its URL
                alb_url = line.replace("\n","")
                newAlbum = Album(alb_url)
                getAlbumID(newAlbum)
                getAlbumDetails(newAlbum, myUser)
                if not os.path.exists(newAlbum.primaryArtist + "/" + newAlbum.albumShortcut):
                   os.makedirs(newAlbum.primaryArtist + "/" + newAlbum.albumShortcut)
                for track in newAlbum.tracks:
                    startPlaybackSession(myUser)
                    getTrackLocation(myUser, track)
                    download(track, newAlbum)
                    track.convertToMP3(newAlbum)
                    #break
    sendNotification()


if __name__ == "__main__":
    main()

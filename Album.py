from Track import Track
import re
class Album:
    def __init__(self, url):
        self.splitURL(url)



    def splitURL(self, url):
        splitted = url.split("/")
        self.url = url
        self.artistShortcut = splitted[4]
        self.albumShortcut = splitted[6]




    def addTracks(self, json_track_infos):
        self.tracks = []
        for track in json_track_infos:
            newTrack = Track()
            newTrack.encodedName = re.sub(r'\W+', '_', track["name"])
            newTrack.displayName = track["name"]
            newTrack.trackId = track["trackId"]
            newTrack.artistName = track["displayArtistName"]
            self.tracks.append(newTrack)
